set -e

bold=$(tput bold)
normal=$(tput sgr0)

echoBold() {
  echo ${bold}$1${normal}
}

DO_WAIT=false
PROCESS_NAME='patientFlowPOC'
#PROCESS_NAME='patientFlowPOC_manual'

wait() {
  sleep 1
}

COUNTER=0
if [[ -f apptId ]]; then
  INSTANCE_ID=$( cat apptId )
  echo "Set instance to ${INSTANCE_ID}"
fi

actionMessage() {
  if [[ 'true' == "${DO_WAIT}" ]]; then wait; fi
  echo
  echoBold "$1"
}

getAppointmentState() {
  actionMessage "Get current incoming lists"
  curl -s http://localhost:8080/appointment/${INSTANCE_ID}
  echo
}

createAppointment() {
  echoBold "Create appointment"

  PATIENT_ID="$2"
  case "${1}" in
    In*)
      START_TIME=$( node -e '
        const time = new Date();
        time.setSeconds( time.getSeconds() + 5 )
        console.log( time.setMinutes( time.getMinutes() + 90 ) )
      ' )
      PATIENT_TYPE='Inpatient'
      ;;
    Out*)
      START_TIME=$( node -e '
        const time = new Date();
        time.setSeconds( time.getSeconds() + 5 )
        console.log( time.setHours( time.getHours() + 24 ) )
      ' )
      PATIENT_TYPE='Outpatient'
      ;;
    *)
      echo 'Must use patient types In / Out'
      exit 1
      ;;
  esac

  actionMessage "${PATIENT_TYPE} selected for $( node -e "console.log( new Date( ${START_TIME} ).toLocaleString() )" )"
  BODY=$(
    jq -cn \
      --argjson startTime "${START_TIME}" \
      --arg patientType "${PATIENT_TYPE}" \
      --arg patientId "${PATIENT_ID}" \
      '{startTime:$startTime,patientType:$patientType,patientId:$patientId}'
  )
  INSTANCE_ID=$(
    curl -sX POST http://localhost:8080/appointment/${PROCESS_NAME}/${PATIENT_ID} \
      --header "Content-Type: application/json" \
      --data "$BODY"
  )
  echo
  echo "Instance Id: ${INSTANCE_ID}"
  echo
  echo ${INSTANCE_ID} > apptId
  getAppointmentState
}

allocateNextBay() {
  actionMessage "Allocating to $1"
  curl -sX POST http://localhost:8080/appointment/${INSTANCE_ID}/allocate \
    --header "Content-Type: application/json" \
    --data "$1"
  getAppointmentState
}

patientArrived() {
  actionMessage "Patient arrived"
  curl -sX POST http://localhost:8080/appointment/${INSTANCE_ID}/arrived
  getAppointmentState
}

modifyTarget() {
  actionMessage "Modifying target location to $1"
  curl -sX POST http://localhost:8080/appointment/${INSTANCE_ID}/modifyTarget \
    --header "Content-Type: application/json" \
    --data "$1"
  getAppointmentState
}


requestMove() {
  actionMessage "Requesting move to $1"
  curl -sX POST http://localhost:8080/appointment/${INSTANCE_ID}/requestMove \
    --header "Content-Type: application/json" \
    --data "$1"
  getAppointmentState
}

directMove() {
  actionMessage "Moving to $1"
  curl -sX POST http://localhost:8080/appointment/${INSTANCE_ID}/directMove \
    --header "Content-Type: application/json" \
    --data "$1"
  getAppointmentState
}

completeMilestone() {
  actionMessage "Completing Milestone $1"
  curl -sX POST http://localhost:8080/appointment/${INSTANCE_ID}/milestone \
    --header "Content-Type: application/json" \
    --data "$1"
  getAppointmentState
}

getHistory() {
  actionMessage "Get incoming list history"
  curl -s http://localhost:8080/appointment/${INSTANCE_ID}/history
  echo
  echo
}

getIncomingList() {
  actionMessage "Get incoming list for $1"
  curl -s "$( echo "http://localhost:8080/unit/$1/incoming" | sed 's/ /%20/g' )"
  echo
  echo
}

allocateRandom() {
  TARGET_NODE=$( node -e "
    let incoming = $( curl -s http://localhost:8080/appointment/${INSTANCE_ID}/json );
    if ( Array.isArray( incoming ) )
      incoming = incoming[ Math.floor( Math.random() * incoming.length ) ];
    console.log( incoming );
  " )
  if [[ "None" == "${TARGET_NODE}" ]]; then
    wait
    echo
    echo "Not on any incoming lists, waiting ${COUNTER}"
    COUNTER=$(( $COUNTER + 1 ))
  else
    allocateNextBay "${TARGET_NODE}"
  fi
}

isComplete() {
  echo $( curl -s http://localhost:8080/appointment/${INSTANCE_ID}/complete )
}

set +e