#!/bin/bash

set -e

cd ..

mvn clean package

cd target

java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -jar workflow-poc-spring-boot-0.0.1-SNAPSHOT.jar

