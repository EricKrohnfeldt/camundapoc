#!/bin/bash

set -e

cd "$( dirname "$0" )"
. setupTest.sh

DO_WAIT='true'

completeMilestoneLater() {
  sleep "$2"
  if [[ "false" == $( isComplete ) ]]; then
    completeMilestone "$1"
  fi
}

#createAppointment 'Out'
createAppointment $( node -e 'console.log( Math.floor( Math.random() * 2 ) == 0 ? "In" : "Out" )' )

completeMilestoneLater "milestonePACU" "20" &
completeMilestoneLater "milestonePhaseII" "30" &

while [[ "false" == $( isComplete ) ]]
do
  allocateRandom
done

getHistory

echo -e '\nDone\n'
