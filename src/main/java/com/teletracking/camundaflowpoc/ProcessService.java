package com.teletracking.camundaflowpoc;

import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.runtime.ActivityInstance;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceModificationBuilder;
import org.camunda.bpm.engine.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
class ProcessService {

    private static final Logger log = LoggerFactory.getLogger( ProcessService.class );

    private final RuntimeService runtimeService;
    private final TaskService taskService;
    private final QueryService queryService;

    ProcessService( RuntimeService runtimeService, TaskService taskService, QueryService queryService ) {
        this.runtimeService = runtimeService;
        this.taskService = taskService;
        this.queryService = queryService;
    }

    String createAppointment( String processKey, AppointmentRequest appointment ) {
        Map<String, Object> initialValues = buildProcessValues( appointment );
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey( processKey, initialValues );
        return processInstance.getProcessInstanceId();
    }

    private Map<String, Object> buildProcessValues( AppointmentRequest appointment ) {
        Map<String, Object> values = new ConcurrentHashMap<>();
        values.put( "patientId", appointment.getPatientId() );
        values.put( "startTime", appointment.getTime() );
        values.put( "patientType", appointment.getPatientType() );
        return values;
    }

    boolean isComplete( String instanceId ) {
        return queryService.isComplete( instanceId );
    }

    List<String> getOpenTaskNames( String instanceId ) {
        return queryService.getActivities( instanceId )
            .stream()
            .map( ActivityInstance::getActivityName )
            .filter( activity -> activity.startsWith( "Incoming List" ) )
            .map( activity -> activity.substring( "Incoming List ".length() ))
            .collect( Collectors.toList() );
    }

    List<String> getIncomingNames( String unit ) {
        return queryService.getOpenActivitiesForUnit( unit )
            .stream()
            .map( HistoricActivityInstance::getProcessInstanceId )
            .map( queryService::getPatientName )
            .collect( Collectors.toList() );
    }


    void sendAllocateMessage( String instanceId, String unitName ) {
        String activityId = queryService.getTaskId( instanceId, "Incoming List " + unitName );
        cancelAllActivityExcept( instanceId, activity -> activityId.equals( activity.getActivityId() ) );

        sendMessage( instanceId, unitName + " Allocated" );
    }

    void sendArrivalMessage( String instanceId ) {
        sendMessage( instanceId, "Arrived" );
    }

    private void sendMessage( String instanceId, String messageName ) {
        List<Execution> executions = queryService.getMessageWaiting( instanceId, messageName );
        if ( executions.isEmpty() )
            throw new IllegalStateException( "Appoint not listening for " + messageName );
        executions
            .forEach(
                execution -> {
                    String executionId = execution.getId();
                    log.debug( "Found waiting execution " + executionId );
                    runtimeService.messageEventReceived( messageName, executionId );
                }
            );
    }

    String getHistory( String instanceId ) {
        List<HistoricActivityInstance> history = queryService.getHistory( instanceId );
        return history.isEmpty() ?
            "No History Found" :
            history
                .stream()
                .peek( activity -> log.debug( "History Event: " + activity.getActivityId() + " / " + activity.getActivityType() ) )
                //.filter( history -> HiddenSteps.shouldDisplay( history.getActivityType() ) )
                .map( ProcessService::getActivityHistoryString )
                .collect( Collectors.joining( CamundaPoc.multilineDivider ) );
    }

    private static String getActivityHistoryString( HistoricActivityInstance history ) {
        String eventName = Optional.ofNullable( history.getActivityName() ).orElseGet( history::getActivityType );
        return Optional.ofNullable( history.getEndTime() )
            .map( endTime ->
                eventName + "  " + history.getDurationInMillis() +
                    "ms  -> " + history.getStartTime() + "  -  " + history.getEndTime()
            )
            .orElseGet( () ->
                eventName + "  " + history.getStartTime() + "  -  On Going"
            );
    }

    void modifyTarget( String instanceId, String unitName ) {
        ProcessInstanceModificationBuilder builder = buildCancelAllActivity( instanceId );
        builder.startBeforeActivity( queryService.getTaskId( instanceId, "Incoming List " + unitName ) );
        builder.execute();
    }

    void requestMove( String instanceId, String unitName ) {
        ProcessInstanceModificationBuilder builder = buildCancelAllActivity( instanceId );
        builder.startAfterActivity( queryService.getTaskId( instanceId, "Incoming List " + unitName ) );
        builder.execute();
    }

    void directMove( String instanceId, String unitName ) {
        ProcessInstanceModificationBuilder builder = buildCancelAllActivity( instanceId );
        builder.startAfterActivity( queryService.getTaskId( instanceId, "Allocated at " + unitName ) );
        builder.execute();
    }

    void completeMilestone( String instanceId, String milestoneName ) {
        runtimeService.setVariable( instanceId, milestoneName, true );
    }

    private void cancelAllActivityExcept( String instanceId, Predicate<ActivityInstance> exception ) {
        buildCancelAllActivityExcept( instanceId, exception ).execute();
    }

    private ProcessInstanceModificationBuilder buildCancelAllActivity( String instanceId ) {
        return buildCancelAllActivityExcept( instanceId, x -> false );
    }

    private ProcessInstanceModificationBuilder buildCancelAllActivityExcept( String instanceId, Predicate<ActivityInstance> exception ) {
        ProcessInstanceModificationBuilder builder = runtimeService.createProcessInstanceModification( instanceId );
        queryService.getActivities( instanceId )
            .stream()
            .filter( x -> ! exception.test( x ) )
            .forEach( activity -> {
                log.debug( "Canceling " + activity.getActivityType() + " activity " + activity.getActivityId() + " - " + activity.getActivityName() );
                builder.cancelAllForActivity( activity.getActivityId() );
            });
        return builder;
    }

}
