package com.teletracking.camundaflowpoc;

import java.time.Instant;

class AppointmentRequest {

    private long startTime;
    private String patientId;
    private String patientType;

    Instant getTime() {
        return Instant.ofEpochMilli( startTime );
    }

    public String getPatientId() {
        return patientId;
    }

    public String getPatientType() {
        return patientType.toLowerCase().startsWith( "in" ) ? "INPATIENT" : "OUTPATIENT";
    }

    public long getStartTime() {
        return startTime;
    }

}
