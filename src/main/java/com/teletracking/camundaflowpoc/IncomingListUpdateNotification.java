package com.teletracking.camundaflowpoc;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IncomingListUpdateNotification implements JavaDelegate {

    // com.teletracking.camundaflowpoc.IncomingListUpdateNotification

    private static final Logger log = LoggerFactory.getLogger( IncomingListUpdateNotification.class );

    @Override
    public void execute( DelegateExecution delegateExecution ) throws Exception {
        String processId = delegateExecution.getProcessInstanceId();
        String patientId = ( String ) delegateExecution.getProcessEngineServices().getRuntimeService().getVariable(
            processId,
            "patientId"
        );
        log.info( "Appointment ( " + processId + " ) has become available for patient id " + patientId );
    }

}
