package com.teletracking.camundaflowpoc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping( "/appointment" )
class AppointmentController {

    private static final Logger log = LoggerFactory.getLogger( AppointmentController.class );

    private final ProcessService processService;

    AppointmentController( ProcessService processService ) {
        this.processService = processService;
    }

    @PostMapping( "/{processKey}/{patientId}" )
    String createPatientAppointment( @PathVariable String processKey, @PathVariable String patientId, @RequestBody AppointmentRequest appointment ) {
        log.trace( "Read appoint time as " + appointment.getTime() );
        log.debug( "Creating " + processKey + " appointment for " + patientId + " at " + formatTime( appointment.getTime() ) );
        String processId = processService.createAppointment( processKey, appointment );
        log.info( "Created " + processKey + " appointment for " + patientId + " at " + formatTime( appointment.getTime() ) + " with id " + processId );
        return processId;
    }

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern( "MM-dd-yyyy hh:mm:ss a" );

    private String formatTime( Instant time ) {
        return LocalDateTime.ofInstant( time, ZoneId.systemDefault() ).format( dateTimeFormatter );
    }

    @GetMapping( "/{instanceId}" )
    String getInstanceState( @PathVariable String instanceId ) {
        try {
            if ( processService.isComplete( instanceId ) )
                return "Complete";
            List<String> tasks = processService.getOpenTaskNames( instanceId );
            return tasks.isEmpty() ?
                "None" :
                String.join( CamundaPoc.multilineDivider, tasks );
        }
        catch( IllegalStateException e ) {
            return "None";
        }
    }

    @GetMapping( "/{instanceId}/json" )
    String getInstanceStateJson( @PathVariable String instanceId ) {
        try {
            List<String> tasks = processService.getOpenTaskNames( instanceId );
            return tasks.isEmpty() ?
                "'None'" :
                "['" + String.join( "','", tasks ) + "']";
        }
        catch( IllegalStateException e ) {
            return "'None'";
        }
    }

    @GetMapping( "/{instanceId}/complete" )
    boolean getInstanceComplete( @PathVariable String instanceId ) {
        try { return processService.isComplete( instanceId ); }
        catch( IllegalStateException e ) { return true; }
    }

    @GetMapping( "/{instanceId}/history" )
    String getHistory( @PathVariable String instanceId ) {
        return processService.getHistory( instanceId );
    }

    @PostMapping( "/{instanceId}/allocate" )
    String allocateLocation( @PathVariable String instanceId, @RequestBody String unitName ) {
        return wrapCommand(
            () -> processService.sendAllocateMessage( instanceId, unitName )
        );
    }

    @PostMapping( "/{instanceId}/arrived" )
    String patientArrived( @PathVariable String instanceId ) {
        return wrapCommand(
            () -> processService.sendArrivalMessage( instanceId )
        );
    }

    @PostMapping( "/{instanceId}/modifyTarget" )
    String modifyTarget( @PathVariable String instanceId, @RequestBody String unitName ) {
        return wrapCommand(
            () -> processService.modifyTarget( instanceId, unitName )
        );
    }

    @PostMapping( "/{instanceId}/requestMove" )
    String requestMove( @PathVariable String instanceId, @RequestBody String unitName ) {
        return wrapCommand(
            () -> processService.requestMove( instanceId, unitName )
        );
    }

    @PostMapping( "/{instanceId}/directMove" )
    String directMove( @PathVariable String instanceId, @RequestBody String unitName ) {
        return wrapCommand(
                () -> processService.directMove( instanceId, unitName )
        );
    }

    @PostMapping( "/{instanceId}/milestone" )
    String completeMilestone( @PathVariable String instanceId, @RequestBody String milestoneName ) {
        return wrapCommand(
            () -> processService.completeMilestone( instanceId, "milestone" + milestoneName )
        );
    }

    private String wrapCommand( Runnable command ) {
        try {
            command.run();
            return "Success";
        }
        catch( IllegalStateException e ) {
            return "Bad request: " + e.getMessage();
        }

    }


}
