package com.teletracking.camundaflowpoc;

import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.ActivityInstance;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.model.bpmn.instance.BaseElement;
import org.camunda.bpm.model.bpmn.instance.FlowElement;
import org.camunda.bpm.model.bpmn.instance.UserTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
class QueryService {

    private static final Logger log = LoggerFactory.getLogger( QueryService.class );

    private final RuntimeService runtimeService;
    private final TaskService taskService;
    private final RepositoryService repositoryService;
    private final HistoryService historyService;

    QueryService( RuntimeService runtimeService, TaskService taskService, RepositoryService repositoryService, HistoryService historyService ) {
        this.runtimeService = runtimeService;
        this.taskService = taskService;
        this.repositoryService = repositoryService;
        this.historyService = historyService;
    }

    boolean isComplete( String instanceId ) {
        ProcessInstance instance = getProcessInstance( instanceId );
        return instance.isEnded();
    }

    @Deprecated
    List<Task> getOpenTasksForProcess( String instanceId ) {
        return taskService.createTaskQuery()
            .processInstanceId( instanceId )
            .active()
            .list();
    }

    @Deprecated
    List<Task> getOpenTasksForUnit( String unit ) {
        return taskService.createTaskQuery()
            .taskName( unit )
            .active()
            .list();
    }

    List<HistoricActivityInstance> getOpenActivitiesForUnit( String unit ) {
        return historyService.createHistoricActivityInstanceQuery()
            .activityName( "Incoming List " + unit )
            .unfinished()
            .list();
    }

//    String getActivityName( Task task ) {
//        return Optional.ofNullable(
//            repositoryService.getBpmnModelInstance( task.getProcessDefinitionId() )
//                .getModelElementById( task.getTaskDefinitionKey() )
//                .getAttributeValue( "name" )
//        ).orElseGet( task::getTaskDefinitionKey );
//    }

    String getTaskId( String instanceId, String taskName ) {
        ProcessInstance instance = getProcessInstance( instanceId );
        ProcessDefinition definition = repositoryService.createProcessDefinitionQuery()
            .processDefinitionId( instance.getProcessDefinitionId() )
            .singleResult();

        return repositoryService.getBpmnModelInstance( definition.getId() )
            .getModelElementsByType( FlowElement.class )
            .stream()
            .filter( element -> taskName.equals( element.getName() ) )
            .findFirst()
            .map( BaseElement::getId )
            .orElseThrow( () -> new IllegalStateException( "Unable to find task " + taskName ) );
    }

    List<ActivityInstance> getActivities( String instanceId ) {
        log.warn( "getActivities does not consider sub processes" );
        return Arrays.asList( runtimeService.getActivityInstance( instanceId ).getChildActivityInstances() );
    }

    List<HistoricActivityInstance> getHistory( String instanceId ) {
        return historyService.createHistoricActivityInstanceQuery()
            .processInstanceId( instanceId )
            .orderByHistoricActivityInstanceStartTime()
            .asc()
            .list();
    }

    ProcessInstance getProcessInstance( String instanceId ) {
        return Optional.ofNullable(
            runtimeService.createProcessInstanceQuery()
                .processInstanceId( instanceId )
                .singleResult()
        ).orElseThrow( () -> new IllegalStateException( "Unable to find appointment process instance" ) );
    }

    List<Execution> getMessageWaiting( String instanceId, String activityName ) {
        return runtimeService.createExecutionQuery()
            .processInstanceId( instanceId )
            .messageEventSubscriptionName( activityName )
            .list();
    }

    String getPatientName( String instanceId ) {
        return runtimeService.getVariable( instanceId, "patientId" ).toString();
    }

}
