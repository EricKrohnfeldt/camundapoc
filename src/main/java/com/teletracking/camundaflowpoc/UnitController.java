package com.teletracking.camundaflowpoc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping( "/unit" )
class UnitController {

    private static final Logger log = LoggerFactory.getLogger( UnitController.class );

    private final ProcessService processService;

    UnitController( ProcessService processService ) {
        this.processService = processService;
    }

    @GetMapping( "/{unit}/incoming")
    String getIncomingList( @PathVariable String unit ) {
        try {
            List<String> names = processService.getIncomingNames( unit );
            return names.isEmpty() ?
                "None" :
                String.join( CamundaPoc.multilineDivider, names );
        }
        catch( IllegalStateException e ) {
            return "None";
        }
    }

}
