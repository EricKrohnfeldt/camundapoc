package com.teletracking.camundaflowpoc;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication( scanBasePackages = "com.teletracking.camundaflowpoc" )
@EnableProcessApplication
class CamundaPoc {

    //public static final String multilineDivider = "\n---\n";
    public static final String multilineDivider = "\n";

    public static void main( String... args ) {
        SpringApplication.run( CamundaPoc.class, args );
    }

    @Bean
    public CommandLineRunner init() {
        return strings -> System.out.println( "\n\nCamunda Poc Ready\n" );
    }

}
